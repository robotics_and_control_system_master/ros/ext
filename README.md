[[_TOC_]]

## WEB TOOL

A ROS application running from a web browser

Publications:

- ```/device/rotation``` (geometry_msgs/Vector3)
- ```/device/acceleration``` (geometry_msgs/Vector3)
- ```/device/screen_orientation``` (std_msgs/Int32)

  Rotation and acceleration information are described in [developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Web/Events/Orientation_and_motion_data_explained). Screen orientation takes values in the range {0, 90, 180 -90} degrees

Subscriptions:

- ```/camera/image``` (sensor_msgs/CompressedImage encoded as JPG)

  An image published to this topic will be shown in the screen

Services (client)

- ```/device/service1``` (std_srvs/Trigger)
- ```/device/service2``` (std_srvs/Trigger)

### Setup

To configure the project go into the ```src``` folder and run ```setup.bash```

```bash
bash setup.bash
```

### Launch the web tool

1) Source the workspace and launch the web and ros bridge server

    ```bash
    roslaunch web_tool web_server.launch
    roslaunch web_tool ros_bridge.launch
    ```

2) Allow websocket connections in the browser by openning the following URL (replace ```HOST_IP``` with the IP address of the host)

    ```
    https://HOST_IP:55001/
    ```

    Ignore the browser warning message about *Potential Security Risk* by selecting **Advanced... > Accept the Risk and Continue**. Note: the warning message appears because we are using a self-signed digital certificate

3) Finally, open the web tool under the following URL (replace ```HOST_IP``` with the IP address of the host)

    ```
    https://HOST_IP:55000/
    ```

    Ignore the browser warning message about *Potential Security Risk* by selecting **Advanced... > Accept the Risk and Continue**. Note: the warning message appears because we are using a self-signed digital certificate
